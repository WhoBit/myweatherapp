﻿namespace MyWeatherApp
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnParse = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblWeather = new System.Windows.Forms.Label();
            this.lblHumidity = new System.Windows.Forms.Label();
            this.lblWinds = new System.Windows.Forms.Label();
            this.lblPrecip = new System.Windows.Forms.Label();
            this.lblTemperature = new System.Windows.Forms.Label();
            this.lblWindSpeed = new System.Windows.Forms.Label();
            this.txtWeather = new System.Windows.Forms.TextBox();
            this.txtTemperature = new System.Windows.Forms.TextBox();
            this.txtHumidity = new System.Windows.Forms.TextBox();
            this.txtWinds = new System.Windows.Forms.TextBox();
            this.txtWindSpeed = new System.Windows.Forms.TextBox();
            this.txtPrecip = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblCredit = new System.Windows.Forms.Label();
            this.txtOutput = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnParse
            // 
            this.btnParse.Location = new System.Drawing.Point(448, 12);
            this.btnParse.Name = "btnParse";
            this.btnParse.Size = new System.Drawing.Size(94, 23);
            this.btnParse.TabIndex = 0;
            this.btnParse.Text = "Get Weather";
            this.btnParse.UseVisualStyleBackColor = true;
            this.btnParse.Click += new System.EventHandler(this.btnParse_Click);
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(467, 49);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(105, 9);
            this.txtDate.Name = "txtDate";
            this.txtDate.Size = new System.Drawing.Size(252, 20);
            this.txtDate.TabIndex = 3;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(28, 12);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(33, 13);
            this.lblDate.TabIndex = 4;
            this.lblDate.Text = "Date:";
            // 
            // lblWeather
            // 
            this.lblWeather.AutoSize = true;
            this.lblWeather.Location = new System.Drawing.Point(28, 55);
            this.lblWeather.Name = "lblWeather";
            this.lblWeather.Size = new System.Drawing.Size(48, 13);
            this.lblWeather.TabIndex = 5;
            this.lblWeather.Text = "Weather";
            // 
            // lblHumidity
            // 
            this.lblHumidity.AutoSize = true;
            this.lblHumidity.Location = new System.Drawing.Point(27, 119);
            this.lblHumidity.Name = "lblHumidity";
            this.lblHumidity.Size = new System.Drawing.Size(47, 13);
            this.lblHumidity.TabIndex = 7;
            this.lblHumidity.Text = "Humidity";
            // 
            // lblWinds
            // 
            this.lblWinds.AutoSize = true;
            this.lblWinds.Location = new System.Drawing.Point(27, 150);
            this.lblWinds.Name = "lblWinds";
            this.lblWinds.Size = new System.Drawing.Size(37, 13);
            this.lblWinds.TabIndex = 8;
            this.lblWinds.Text = "Winds";
            // 
            // lblPrecip
            // 
            this.lblPrecip.AutoSize = true;
            this.lblPrecip.Location = new System.Drawing.Point(27, 210);
            this.lblPrecip.Name = "lblPrecip";
            this.lblPrecip.Size = new System.Drawing.Size(77, 13);
            this.lblPrecip.TabIndex = 9;
            this.lblPrecip.Text = "Today\'s Precip";
            // 
            // lblTemperature
            // 
            this.lblTemperature.AutoSize = true;
            this.lblTemperature.Location = new System.Drawing.Point(27, 86);
            this.lblTemperature.Name = "lblTemperature";
            this.lblTemperature.Size = new System.Drawing.Size(67, 13);
            this.lblTemperature.TabIndex = 6;
            this.lblTemperature.Text = "Temperature";
            // 
            // lblWindSpeed
            // 
            this.lblWindSpeed.AutoSize = true;
            this.lblWindSpeed.Location = new System.Drawing.Point(28, 182);
            this.lblWindSpeed.Name = "lblWindSpeed";
            this.lblWindSpeed.Size = new System.Drawing.Size(66, 13);
            this.lblWindSpeed.TabIndex = 10;
            this.lblWindSpeed.Text = "Wind Speed";
            // 
            // txtWeather
            // 
            this.txtWeather.Location = new System.Drawing.Point(105, 52);
            this.txtWeather.Name = "txtWeather";
            this.txtWeather.Size = new System.Drawing.Size(252, 20);
            this.txtWeather.TabIndex = 11;
            // 
            // txtTemperature
            // 
            this.txtTemperature.Location = new System.Drawing.Point(105, 86);
            this.txtTemperature.Name = "txtTemperature";
            this.txtTemperature.Size = new System.Drawing.Size(252, 20);
            this.txtTemperature.TabIndex = 12;
            // 
            // txtHumidity
            // 
            this.txtHumidity.Location = new System.Drawing.Point(105, 119);
            this.txtHumidity.Name = "txtHumidity";
            this.txtHumidity.Size = new System.Drawing.Size(252, 20);
            this.txtHumidity.TabIndex = 13;
            // 
            // txtWinds
            // 
            this.txtWinds.Location = new System.Drawing.Point(105, 150);
            this.txtWinds.Name = "txtWinds";
            this.txtWinds.Size = new System.Drawing.Size(252, 20);
            this.txtWinds.TabIndex = 14;
            // 
            // txtWindSpeed
            // 
            this.txtWindSpeed.Location = new System.Drawing.Point(105, 179);
            this.txtWindSpeed.Name = "txtWindSpeed";
            this.txtWindSpeed.Size = new System.Drawing.Size(252, 20);
            this.txtWindSpeed.TabIndex = 15;
            // 
            // txtPrecip
            // 
            this.txtPrecip.Location = new System.Drawing.Point(105, 207);
            this.txtPrecip.Name = "txtPrecip";
            this.txtPrecip.Size = new System.Drawing.Size(252, 20);
            this.txtPrecip.TabIndex = 16;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(69, 294);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(303, 107);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // lblCredit
            // 
            this.lblCredit.AutoSize = true;
            this.lblCredit.Location = new System.Drawing.Point(65, 263);
            this.lblCredit.Name = "lblCredit";
            this.lblCredit.Size = new System.Drawing.Size(105, 13);
            this.lblCredit.TabIndex = 18;
            this.lblCredit.Text = "All Data Courtesy Of:";
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(396, 296);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.Size = new System.Drawing.Size(140, 105);
            this.txtOutput.TabIndex = 19;
            this.txtOutput.Visible = false;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 415);
            this.Controls.Add(this.txtOutput);
            this.Controls.Add(this.lblCredit);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtPrecip);
            this.Controls.Add(this.txtWindSpeed);
            this.Controls.Add(this.txtWinds);
            this.Controls.Add(this.txtHumidity);
            this.Controls.Add(this.txtTemperature);
            this.Controls.Add(this.txtWeather);
            this.Controls.Add(this.lblWindSpeed);
            this.Controls.Add(this.lblPrecip);
            this.Controls.Add(this.lblWinds);
            this.Controls.Add(this.lblTemperature);
            this.Controls.Add(this.lblHumidity);
            this.Controls.Add(this.lblWeather);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.txtDate);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnParse);
            this.Name = "frmMain";
            this.Text = "Current Weather - Akron, PA";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnParse;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblWeather;
        private System.Windows.Forms.Label lblHumidity;
        private System.Windows.Forms.Label lblWinds;
        private System.Windows.Forms.Label lblPrecip;
        private System.Windows.Forms.Label lblTemperature;
        private System.Windows.Forms.Label lblWindSpeed;
        private System.Windows.Forms.TextBox txtWeather;
        private System.Windows.Forms.TextBox txtTemperature;
        private System.Windows.Forms.TextBox txtHumidity;
        private System.Windows.Forms.TextBox txtWinds;
        private System.Windows.Forms.TextBox txtWindSpeed;
        private System.Windows.Forms.TextBox txtPrecip;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblCredit;
        private System.Windows.Forms.TextBox txtOutput;
    }
}

