﻿using System;
using System.Windows.Forms;

namespace MyWeatherApp
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void btnParse_Click(object sender, EventArgs e)
        {
            RestClient rClient = new RestClient();

            rClient.endPoint = "http://api.wunderground.com/api/986a11a4934ba7bc/geolookup/conditions/q/PA/Akron.json";

            string strResponse = string.Empty;

            strResponse = rClient.makeRequest();
            
            parseForecast(strResponse);
        }

        private void reportIt(string strDebugText)
        {
            try
            {
                txtOutput.Text = txtOutput.Text + strDebugText + Environment.NewLine;
                txtOutput.SelectionStart = txtOutput.TextLength;
                txtOutput.ScrollToCaret();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write(ex.Message, ToString() + Environment.NewLine);
            }
        }

        private void parseForecast(string MyString)
        {
            string[] result;
            string[] splitCharacters = new string[] { "\n" };
            result = MyString.Split(splitCharacters, StringSplitOptions.None);
            parseArray(result);            
        }

        private void parseArray(string[] MyArray)
        {

            bool enablePrint = false;            

            for (int i = 0; i < MyArray.Length -1 ; i++)
            {
                string s = MyArray[i];
                if (s.Contains("nowcast"))
                {
                    enablePrint = false;
                    reportIt("Disabling at line count " + i.ToString());
                    break;
                }

                if (enablePrint)
                {
                    
                    if (s.Contains("observation_time\""))
                    {
                        txtDate.Text = getValue(s);
                    }
                    if (s.Contains("weather\""))
                    {
                        txtWeather.Text = getValue(s);
                    }
                    if (s.Contains("temperature_string\""))
                    {
                        txtTemperature.Text = getValue(s);
                    }
                    if (s.Contains("relative_humidity\""))
                    {
                        txtHumidity.Text = getValue(s);
                    }
                    if (s.Contains("wind_string\""))
                    {
                        txtWinds.Text = getValue(s);
                    }
                    if (s.Contains("wind_mph\""))
                    {
                        txtWindSpeed.Text = getValue(s);
                    }
                    if (s.Contains("precip_today_in\""))
                    {
                        txtPrecip.Text = getValue(s);
                    }
                    
                }

                if (s.Contains("estimated"))
                {
                    enablePrint = true;
                    reportIt("Enabling at line count " + i.ToString());
                }                
            }
        }

        private string getValue(string MyString)
        {
            string[] result;
            string[] splitCharacters = new string[] { "\":\"","\"" };
            result = MyString.Split(splitCharacters, StringSplitOptions.None);
            return result[2];
        }

        private void parseString(string MyString)
        {
            string[] result;
            string[] splitCharacters = new string[] { ":" };

            result = MyString.Split(splitCharacters, StringSplitOptions.None);

            int lineCounter = 0;

            reportIt("Looking for keyword...");

            foreach (string s in result)
            {
                lineCounter++;
                if (s.Contains("estimated"))
                {
                    reportIt(s);
                    reportIt(lineCounter.ToString());
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
