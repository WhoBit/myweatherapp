2018-01-21

I think I got the RestClient.cs code from a course on Lynda.com:
https://www.lynda.com/C-tutorials/Code-Clinic-C/161815-2.html

I was also trying to figure out a good way to parse Json data.  As 
a result, there is a folder names JsonParser.  I still need to work
with that.  At this point, it is pretty much brute force parsing
of the Json returned from the Rest Client.  Hopefully it is a
decent start.

At this point, it only shows the current weather for Akron, PA.

-Kevin


